package com.mlethe.library.fingerprint;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;

import com.mlethe.library.fingerprint.callback.FingerprintCallback;
import com.mlethe.library.fingerprint.callback.IFingerprint;
import com.mlethe.library.fingerprint.entity.VerificationDialogStyle;
import com.mlethe.library.fingerprint.uitls.AndroidVersionUtil;

/**
 * 指纹验证管理类
 *
 * @author Mlethe
 * @date 2019/7/9
 */
public class FingerprintBuilder {

    /**
     * 必选字段
     */
    private Context context;
    private FingerprintCallback callback;

    /**
     * 可选字段
     */
    private int cancelTextColor;
    private int fingerprintColor;

    /**
     * 在Android 9.0系统上，是否开启google提供的验证方式及验证框
     */
    private boolean enableAndroidP;
    /**
     * 取消默认弹窗
     */
    private boolean cancelDialog;
    private String title;
    private String subTitle;
    private String description;
    /**
     * 取消按钮文字
     */
    private String cancelBtnText;

    /**
     * 是否是设置指纹
     */
    private boolean isSetting;

    /**
     * 构建器
     *
     * @param activity
     */
    public FingerprintBuilder(Activity activity) {
        this.context = activity;
    }

    /**
     * 指纹识别回调
     *
     * @param callback
     */
    public FingerprintBuilder callback(FingerprintCallback callback) {
        this.callback = callback;
        return this;
    }

    /**
     * 取消按钮文本色
     *
     * @param color
     */
    public FingerprintBuilder cancelTextColor(int color) {
        this.cancelTextColor = color;
        return this;
    }

    /**
     * 指纹图标颜色
     *
     * @param color
     */
    public FingerprintBuilder fingerprintColor(int color) {
        this.fingerprintColor = color;
        return this;
    }

    /**
     * 在 >= Android 9.0 系统上，是否开启google提供的验证方式及验证框
     *
     * @param enableAndroidP
     */
    public FingerprintBuilder enableAndroidP(boolean enableAndroidP) {
        this.enableAndroidP = enableAndroidP;
        return this;
    }

    /**
     * 取消弹窗
     *
     * @param cancelDialog
     */
    public FingerprintBuilder setCancelDialog(boolean cancelDialog) {
        this.cancelDialog = cancelDialog;
        return this;
    }

    /**
     * >= Android 9.0 的验证框的主标题
     *
     * @param title
     */
    public FingerprintBuilder title(String title) {
        this.title = title;
        return this;
    }

    /**
     * >= Android 9.0 的验证框的副标题
     *
     * @param subTitle
     */
    public FingerprintBuilder subTitle(String subTitle) {
        this.subTitle = subTitle;
        return this;
    }

    /**
     * >= Android 9.0 的验证框的描述内容
     *
     * @param description
     */
    public FingerprintBuilder description(String description) {
        this.description = description;
        return this;
    }

    /**
     * >= Android 9.0 的验证框的取消按钮的文字
     *
     * @param cancelBtnText
     */
    public FingerprintBuilder cancelBtnText(String cancelBtnText) {
        this.cancelBtnText = cancelBtnText;
        return this;
    }

    /**
     * 是否是设置指纹
     * @param setting true 设置指纹
     * @return
     */
    public FingerprintBuilder setSetting(boolean setting) {
        this.isSetting = setting;
        return this;
    }

    /**
     * 开始构建
     *
     * @return
     */
    public IFingerprint build() {
        if (context == null) {
            return null;
        }
        IFingerprint fingerprint = null;
        if (AndroidVersionUtil.isAboveAndroidP()) {
            if (cancelDialog) {
                fingerprint = FingerprintImplForNoDialog.getInstance();
            } else if (enableAndroidP) {
                fingerprint = FingerprintImplForAndroidP.getInstance();
            } else {
                fingerprint = FingerprintImplForAndroidM.getInstance();
            }
        } else if (AndroidVersionUtil.isAboveAndroidM()) {
            if (cancelDialog) {
                fingerprint = FingerprintImplForNoDialog.getInstance();
            } else {
                fingerprint = FingerprintImplForAndroidM.getInstance();
            }
        } else {
            // Android 6.0 以下官方未开放指纹识别，某些机型自行支持的情况暂不做处理
            if (callback != null) {
                callback.onHwUnavailable();
            }
            onDestroy();
            return null;
        }
        // 检测指纹硬件是否存在或者是否可用，若false，不再弹出指纹验证框
        if (!fingerprint.canAuthenticate(context)) {
            if (callback != null) {
                callback.onHwUnavailable();
            }
            onDestroy();
            return null;
        }
        if (fingerprint.createCryptoObject(isSetting)) {
            // 指纹有变化
            if (callback != null) {
                callback.onChange();
            }
            onDestroy();
            return null;
        }
        if (!cancelDialog) {
            /*
             * 设定指纹验证框的样式
             */
            // >= Android 6.0
            VerificationDialogStyle bean = new VerificationDialogStyle();
            bean.setCancelTextColor(cancelTextColor);
            bean.setFingerprintColor(fingerprintColor);

            // >= Android 9.0
            bean.setTitle(title);
            bean.setSubTitle(subTitle);
            bean.setDescription(description);
            bean.setCancelBtnText(cancelBtnText);
            fingerprint.setVerificationDialogStyle(bean);
        }
        fingerprint.setFingerprintCallback(callback);
        fingerprint.authenticate(context);
        onDestroy();
        return fingerprint;
    }

    private void onDestroy() {
        context = null;
        callback = null;
        title = null;
        subTitle = null;
        description = null;
        cancelBtnText = null;
    }
}
