package com.mlethe.library.fingerprint.uitls;

import android.os.Build;

/**
 *
 * @author Mlethe
 * @date 2019/7/9
 */
public class AndroidVersionUtil {
    /**
     * 高于Android P（9.0）
     *
     * @return
     */
    public static boolean isAboveAndroidP() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.P;
    }

    /**
     * 高于Android M（6.0）
     *
     * @return
     */
    public static boolean isAboveAndroidM() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }
}
