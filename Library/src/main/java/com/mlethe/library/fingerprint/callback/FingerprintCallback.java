package com.mlethe.library.fingerprint.callback;

/**
 * 验证结果回调，供使用者调用
 *
 * @author Mlethe
 * @date 2019/7/9
 */
public interface FingerprintCallback {

    /**
     * 无指纹硬件或者指纹硬件不可用
     */
    void onHwUnavailable();

    /**
     * 未添加指纹
     */
    void onNoneEnrolled();

    /**
     * 验证成功
     */
    void onSucceeded();

    /**
     * 指纹库有变化
     */
    void onChange();

    /**
     * 指纹验证错误
     *
     * @param msg 失败信息
     */
    void onFailed(String msg);

    /**
     * 指纹验证失败
     *
     * @param msg 错误信息
     */
    void onError(String msg);

    /**
     * 指纹验证取消
     */
    void onCancel();

}
