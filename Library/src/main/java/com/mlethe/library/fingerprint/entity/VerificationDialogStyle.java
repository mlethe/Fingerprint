package com.mlethe.library.fingerprint.entity;

/**
 * 验证窗口的样式
 *
 * @author Mlethe
 */
public class VerificationDialogStyle {
    /**
     * android 6.0
     */
    private int cancelTextColor;
    private int fingerprintColor;

    /**
     * android 9.0
     *
     * @return
     */
    private String title;
    private String subTitle;
    private String description;
    /**
     * 取消按钮文字
     */
    private String cancelBtnText;

    public int getCancelTextColor() {
        return cancelTextColor;
    }

    public void setCancelTextColor(int cancelTextColor) {
        this.cancelTextColor = cancelTextColor;
    }

    public int getFingerprintColor() {
        return fingerprintColor;
    }

    public void setFingerprintColor(int fingerprintColor) {
        this.fingerprintColor = fingerprintColor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCancelBtnText() {
        return cancelBtnText;
    }

    public void setCancelBtnText(String cancelBtnText) {
        this.cancelBtnText = cancelBtnText;
    }
}
