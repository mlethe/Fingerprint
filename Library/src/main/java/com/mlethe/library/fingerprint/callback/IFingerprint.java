package com.mlethe.library.fingerprint.callback;

import android.content.Context;

import androidx.annotation.NonNull;

import com.mlethe.library.fingerprint.entity.VerificationDialogStyle;

/**
 * @author Mlethe
 * @date 2019/7/9
 */
public interface IFingerprint {

    /**
     * 创建指纹加密
     *
     * @param isSetting 是否是设置指纹
     * @return
     */
    boolean createCryptoObject(boolean isSetting);

    /**
     * 初始化并调起指纹验证
     *
     * @param context
     */
    void authenticate(@NonNull Context context);

    /**
     * 设置回调监听
     *
     * @param callback
     * @return
     */
    void setFingerprintCallback(FingerprintCallback callback);

    /**
     * 设置弹窗样式
     *
     * @param verificationDialogStyle
     * @return
     */
    void setVerificationDialogStyle(VerificationDialogStyle verificationDialogStyle);

    /**
     * 检测指纹硬件是否可用，及是否添加指纹
     *
     * @param context
     * @return
     */
    boolean canAuthenticate(Context context);

    /**
     * 取消指纹
     */
    void cancel();

    /**
     * 释放内存
     */
    void onDestroy();
}
