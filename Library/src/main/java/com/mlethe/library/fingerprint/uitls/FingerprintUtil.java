package com.mlethe.library.fingerprint.uitls;

import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

/**
 * 描述手机信息的检测对象
 *
 * @author Mlethe
 */
public class FingerprintUtil {

    private static final String SONY = "sony";
    private static final String OPPO = "oppo";
    private static final String HUAWEI = "huawei";
    private static final String HONOR = "honor";
    private static final String ONEPLUS = "oneplus";
    private static final String VIVO = "vivo";

    /**
     * 跳转到指纹页面 或 通知用户去指纹录入
     */
    public static void startFingerprint(Context context) {
        String pcgName;
        String clsName;
        if (compareTextSame(SONY)) {
            pcgName = "com.android.settings";
            clsName = "com.android.settings.Settings$FingerprintEnrollSuggestionActivity";
        } else if (compareTextSame(OPPO)) {
            pcgName = "com.coloros.fingerprint";
            clsName = "com.coloros.fingerprint.FingerLockActivity";
        } else if (compareTextSame(HUAWEI)) {
            pcgName = "com.android.settings";
            clsName = "com.android.settings.fingerprint.FingerprintSettingsActivity";
        } else if (compareTextSame(HONOR)) {
            pcgName = "com.android.settings";
            clsName = "com.android.settings.fingerprint.FingerprintSettingsActivity";
        } else {
            // 后续机型会继续加入的
            // 如果以上判断没有符合该机型，那就跳转到设置界面，让用户自己设置吧
            // 跳转到Settings页面的Intent
            pcgName = "com.android.settings";
            clsName = "com.android.settings.Settings";
        }
        startActivity(context, pcgName, clsName);
    }

    private static void startActivity(Context context, String pcgName, String clsName) {
        if (context != null && !TextUtils.isEmpty(pcgName) && !TextUtils.isEmpty(clsName)) {
            Intent intent = new Intent();
            ComponentName componentName = new ComponentName(pcgName, clsName);
            intent.setAction(Intent.ACTION_VIEW);
            intent.setComponent(componentName);
            context.startActivity(intent);
        }
    }

    /**
     * 对比两个字符串，并且比较字符串是否包含在其中的，并且忽略大小写
     *
     * @param value
     * @return
     */
    private static boolean compareTextSame(String value) {
        return value.toUpperCase().contains(Build.BRAND.toUpperCase());
    }

    /**
     * 是否设置锁屏
     *
     * @param context
     * @return
     */
    public static boolean isKeyguardSecure(Context context) {
        if (context == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            KeyguardManager kp = context.getSystemService(KeyguardManager.class);
            return kp.isKeyguardSecure();
        }
        return false;
    }

    /**
     * 硬件是否支持指纹识别
     *
     * @param context
     * @return
     */
    public static boolean isHardwareDetected(Context context) {
        if (context == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            FingerprintManager fp = getFingerprintManagerOrNull(context);
            if (fp != null) {
                return fp.isHardwareDetected();
            }
        }
        return false;
    }

    /**
     * 是否已添加指纹
     *
     * @param context
     * @return
     */
    public static boolean hasEnrolledFingerprints(Context context) {
        if (context == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            FingerprintManager fp = getFingerprintManagerOrNull(context);
            if (fp != null) {
                return fp.hasEnrolledFingerprints();
            }
        }
        return false;
    }

    /**
     * 获取指纹管理类
     *
     * @param context
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private static FingerprintManager getFingerprintManagerOrNull(@NonNull Context context) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
            return context.getSystemService(FingerprintManager.class);
        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M &&
                context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
            return context.getSystemService(FingerprintManager.class);
        } else {
            return null;
        }
    }
}