package com.mlethe.library.fingerprint.demo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mlethe.library.fingerprint.FingerprintBuilder;
import com.mlethe.library.fingerprint.callback.FingerprintCallback;
import com.mlethe.library.fingerprint.callback.IFingerprint;
import com.mlethe.library.fingerprint.uitls.CipherHelper;
import com.mlethe.library.fingerprint.uitls.FingerprintUtil;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private IFingerprint implForNoDialog;
    private CustomFingerprintDialog fingerprintDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.tvOpenFingerprint).setOnClickListener(this);
        findViewById(R.id.tvFingerprint).setOnClickListener(this);
        findViewById(R.id.tvFingerprintCustom).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tvOpenFingerprint) {
            new FingerprintBuilder(this)
                    .callback(fingerprintCallback)
                    .setSetting(true)
                    .enableAndroidP(true)
                    .build();
//            FingerprintUtil.startFingerprint(this);
        } else if (id == R.id.tvFingerprint) {
            new FingerprintBuilder(this)
                    .callback(fingerprintCallback)
                    .enableAndroidP(true)
                    .build();
//            FingerprintUtil.startFingerprint(this);
        } else if (id == R.id.tvFingerprintCustom) {
            fingerprintDialog = new CustomFingerprintDialog();
            fingerprintDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (implForNoDialog != null) {
                                implForNoDialog.cancel();
                            }
                            fingerprintDialog = null;
                        }
                    });
            fingerprintDialog.showAllowingStateLoss(getSupportFragmentManager());
            implForNoDialog = new FingerprintBuilder(this)
                    .callback(fingerprintCallback)
                    .enableAndroidP(false)
                    .setCancelDialog(true)
                    .build();
        }
    }

    private FingerprintCallback fingerprintCallback = new FingerprintCallback() {
        @Override
        public void onSucceeded() {
            if (fingerprintDialog != null) {
                fingerprintDialog.dismissAllowingStateLoss();
            }
            Toast.makeText(MainActivity.this, "验证成功", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onChange() {
            if (fingerprintDialog != null) {
                fingerprintDialog.dismissAllowingStateLoss();
            }
            Toast.makeText(MainActivity.this, "指纹有变化", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFailed(String msg) {
            if (fingerprintDialog != null) {
                fingerprintDialog.showFail(msg, "#FF5555");
            }
            Log.e("TAG", "onFailed: "+ msg);
            Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(String msg) {
            if (fingerprintDialog != null) {
                fingerprintDialog.showError(msg, "#FF5555", null);
            }
            Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel() {
            if (fingerprintDialog != null) {
                fingerprintDialog.dismissAllowingStateLoss();
            }
            Toast.makeText(MainActivity.this, "取消指纹识别", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onHwUnavailable() {
            if (fingerprintDialog != null) {
                fingerprintDialog.dismissAllowingStateLoss();
            }
            Toast.makeText(MainActivity.this, "此设备不支持指纹识别", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNoneEnrolled() {
            //弹出提示框，跳转指纹添加页面
            AlertDialog.Builder lertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
            lertDialogBuilder.setTitle("提  示")
                    .setMessage("请先添加指纹")
                    .setCancelable(false)
                    .setNegativeButton("确认添加", ((DialogInterface dialog, int which) -> {
                        FingerprintUtil.startFingerprint(MainActivity.this);
                    }
                    ))
                    .setPositiveButton("取 消", ((DialogInterface dialog, int which) -> {
                        dialog.dismiss();
                    }
                    ))
                    .create().show();
        }

    };

    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            super.onBackPressed();
        }
    }
}