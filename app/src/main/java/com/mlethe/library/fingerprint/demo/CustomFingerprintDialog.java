package com.mlethe.library.fingerprint.demo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mlethe.library.fingerprint.FingerprintDialog;

/**
 * @author Mlethe
 */
public class CustomFingerprintDialog extends FingerprintDialog {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_custom_fingerprint, container, false);
    }
}
